Imports System.io
Imports System.Text.Encoding

Public Class CreateLangoddsDocuments

    Public outputFolder As String

    Function GenerateLangodds(ByVal inputname As String) As String

        Dim inn As StreamReader = New StreamReader(inputname, GetEncoding("iso-8859-1"))
        Dim dt As DateTime = File.GetLastWriteTime(inputname)

        Dim outfile As String
        outfile = Path.Combine(outputFolder, Path.GetFileName(inputname) & ".doc")

        Dim out As StreamWriter = New StreamWriter(outfile, False, GetEncoding("iso-8859-1"))

        Dim parts As String()
        Dim l As String

        Dim table As Boolean = False

        out.WriteLine("<html><body>")
        out.WriteLine("<h2>Langoddstips uke " & DatePart(DateInterval.WeekOfYear, dt, FirstDayOfWeek.System, FirstWeekOfYear.System) & "</h2>")
        out.WriteLine("<p><b>Vurdert av (navn), NTB.</b></p><BR>")


        Do While inn.Peek() >= 0
            l = inn.ReadLine()

            parts = l.Split(vbTab)

            'Date header
            If parts(0) <> "" Then

                If table Then
                    out.WriteLine("</table><br>")
                    table = False
                End If

                Dim tmp As String = parts(0)
                tmp = tmp.Replace("S�", "S�ndag")
                tmp = tmp.Replace("Ma", "Mandag")
                tmp = tmp.Replace("Ti", "Tirsdag")
                tmp = tmp.Replace("On", "Onsdag")
                tmp = tmp.Replace("To", "Torsdag")
                tmp = tmp.Replace("Fr", "Fredag")
                tmp = tmp.Replace("L�", "L�rdag")
                tmp = tmp.Insert(tmp.Length - 2, "/")

                out.WriteLine("<font color=""red"">[lov1]</font><BR>")
                out.WriteLine(tmp & "<BR>")
            End If

            'TIme header
            If parts(1) <> "" Then

                If table Then
                    out.WriteLine("</table>")
                    table = False
                End If

                Dim tmp As String = parts(1)
                tmp = tmp.Replace("kl", "Kl.")
                tmp = tmp.Insert(tmp.Length - 2, ".")

                out.WriteLine("<font color=""red"">[lov2]</font><BR>")
                out.WriteLine(tmp & "<BR>")
            End If

            'Start table
            If Not table Then
                out.WriteLine("<font color=""red"">[lt1]</font><BR>")
                out.WriteLine("<table border=1>")
                table = True
            End If

            'Table data
            out.WriteLine("<tr>")
            out.WriteLine("<td align=""right"">" & parts(2) & "</td>")

            Dim s As Integer
            If parts(3) <> "" Then
                out.WriteLine("<td>" & parts(3) & "</td>")
                s = 6
            Else
                out.WriteLine("<td>" & parts(4) & "</td>")
                s = 7
            End If

            'If l.ToUpper().IndexOf("ODDS") > -1 And parts.Length < 10 Then s = 5

            Dim i As Integer
            For i = s To s + 2
                out.WriteLine("<td align=""right"">" & parts(i).Replace(",", ".") & "</td>")
            Next

            i += 2
            If (parts.GetUpperBound(0) >= i) Then
                out.WriteLine("<td>" & parts(i) & "</td>")
            Else
                out.WriteLine("<td></td>")
            End If

            out.WriteLine("<td>H</td><td>U</td><td>B</td>")
            out.WriteLine("</tr>")

        Loop

        If table Then
            out.WriteLine("</table>")
            table = False
        End If

        'Footer
        out.WriteLine("<br><br>")
        out.WriteLine("<font color=""red"">{bi}</font>SPILLEFORSLAG<BR>")
        out.WriteLine("<font color=""red"">{bt}</font><BR>")
        out.WriteLine("Samlet odds: <BR>")
        out.WriteLine("<BR></body></html>")

        out.Close()
        inn.Close()

        Return outfile

    End Function

End Class
